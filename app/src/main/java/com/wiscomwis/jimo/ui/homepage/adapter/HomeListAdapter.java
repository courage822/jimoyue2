package com.wiscomwis.jimo.ui.homepage.adapter;

import android.support.annotation.LayoutRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.jimo.C;
import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.BaseModel;
import com.wiscomwis.jimo.data.model.SearchUser;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.library.adapter.base.BaseQuickAdapter;
import com.wiscomwis.library.adapter.base.BaseViewHolder;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;

import java.util.List;

/**
 * Created by xuzhaole on 2018/1/9.
 */

public class HomeListAdapter extends BaseQuickAdapter<SearchUser, BaseViewHolder> {


    public HomeListAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position, List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            holder.getView(R.id.item_homepage_big_iv_sayhello).setBackgroundResource(R.drawable.say_helloed);
        }
    }

    @Override
    protected void convert(final BaseViewHolder holder, final SearchUser searchUser) {
        if (searchUser != null) {
            ImageView imageView = holder.getView(R.id.item_homepage_iv_avatar);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(searchUser.getIconUrlMiddle())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(imageView).build());
            holder.setText(R.id.item_homepage_tv_nickname, searchUser.getNickName());
            holder.setText(R.id.item_homepage_tv_age_and_height, searchUser.getAge() + mContext.getString(R.string.edit_info_years_old) + "  " + mContext.getString(R.string.distance));
            TextView tv_state = holder.getView(R.id.item_homepage_tv_state);
            TextView tv_big_state = holder.getView(R.id.item_homepage_big_tv_state);
            // 视频
            ImageView iBVideo = holder.getView(R.id.item_homepage_iv_state);
            final ImageView iv_sayhello = holder.getView(R.id.item_homepage_big_iv_sayhello);
            final int status = searchUser.getStatus();
            int onlineStatus = searchUser.getOnlineStatus();
            if (onlineStatus == -1) {
                tv_big_state.setText(mContext.getString(R.string.not_online));
            } else {
                tv_big_state.setText(mContext.getString(R.string.edit_info_status));
            }
            if (searchUser.getVipDays() > 0) {
                holder.getView(R.id.iv_vip).setVisibility(View.VISIBLE);
            } else {
                holder.getView(R.id.iv_vip).setVisibility(View.GONE);
            }
            if (UserPreference.isAnchor()) {
                tv_big_state.setVisibility(View.GONE);
                iv_sayhello.setVisibility(View.GONE);
                iv_sayhello.setBackgroundResource(R.drawable.say_hello);
            } else {
                tv_big_state.setVisibility(View.VISIBLE);
                iv_sayhello.setVisibility(View.GONE);
            }
            int isSayHello = searchUser.getIsSayHello();
            if (isSayHello == 1) {
                iv_sayhello.setBackgroundResource(R.drawable.say_helloed);
            } else {
                iv_sayhello.setBackgroundResource(R.drawable.say_hello);
            }
            iv_sayhello.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, mContext.getString(R.string.hello_success), Toast.LENGTH_SHORT).show();
                    searchUser.setIsSayHello(1);
                    iv_sayhello.setBackgroundResource(R.drawable.say_helloed);
                    notifyItemChanged(holder.getAdapterPosition(), searchUser);
                    ApiManager.sayHello(String.valueOf(searchUser.getUesrId()), new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {

                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {

                        }
                    });
                }
            });
            switch (status) {
                case C.homepage.STATE_FREE:// 空闲
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_free);
                    tv_state.setText(mContext.getString(R.string.invitable));
                    break;
                case C.homepage.STATE_BUSY:// 忙线中
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_busy);
                    tv_state.setText(mContext.getString(R.string.talking));
                    break;
                case C.homepage.STATE_NO_DISTRUB:// 勿扰
                    iBVideo.setVisibility(View.GONE);
                    tv_state.setVisibility(View.GONE);
                    tv_big_state.setText(mContext.getString(R.string.message_state_nodistrub));
                    break;
                default:
                    iBVideo.setVisibility(View.VISIBLE);
                    tv_state.setVisibility(View.VISIBLE);
                    iBVideo.setImageResource(R.drawable.ic_video_free);
                    tv_state.setText(mContext.getString(R.string.invitable));
                    break;
            }
        }
    }
}
