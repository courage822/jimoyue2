package com.wiscomwis.jimo.ui.detail.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.CustomDialogAboutPay;
import com.wiscomwis.jimo.common.ParamsUtils;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.common.VideoHelper;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.BaseModel;
import com.wiscomwis.jimo.data.model.HostInfo;
import com.wiscomwis.jimo.data.model.UserBase;
import com.wiscomwis.jimo.data.model.UserDetail;
import com.wiscomwis.jimo.data.model.UserDetailforOther;
import com.wiscomwis.jimo.data.model.UserMend;
import com.wiscomwis.jimo.data.model.UserPhoto;
import com.wiscomwis.jimo.data.preference.DataPreference;
import com.wiscomwis.jimo.data.preference.PayPreference;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.event.SayHelloEvent;
import com.wiscomwis.jimo.parcelable.BigPhotoParcelable;
import com.wiscomwis.jimo.parcelable.ChatParcelable;
import com.wiscomwis.jimo.parcelable.VideoInviteParcelable;
import com.wiscomwis.jimo.ui.chat.ChatActivity;
import com.wiscomwis.jimo.ui.detail.adapter.UserAlbumPhotoAdapter;
import com.wiscomwis.jimo.ui.detail.contract.UserDetailContract;
import com.wiscomwis.jimo.ui.personalcenter.AuthenticationActivity;
import com.wiscomwis.jimo.ui.photo.BigPhotoActivity;
import com.wiscomwis.library.adapter.MultiTypeRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.dialog.AlertDialog;
import com.wiscomwis.library.dialog.OnDialogClickListener;
import com.wiscomwis.library.util.LaunchHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/6/7.
 */

public class UserDetailPresenter implements UserDetailContract.IPresenter {
    private UserDetailContract.IView mUserDetailView;
    private Context mContext;
    private UserBase userBase;
    private UserAlbumPhotoAdapter mAlbumPhotoAdapter;

    public UserDetailPresenter(UserDetailContract.IView mUserDetailView) {
        this.mUserDetailView = mUserDetailView;
        this.mContext = mUserDetailView.obtainContext();
    }

    @Override
    public void start() {
        mAlbumPhotoAdapter = new UserAlbumPhotoAdapter(mContext, R.layout.album_recyclerview_item);
        mUserDetailView.setAdapter(mAlbumPhotoAdapter);
        mAlbumPhotoAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position, RecyclerViewHolder viewHolder) {
                final List<UserPhoto> adapterDataList = mAlbumPhotoAdapter.getAdapterDataList();
                if (adapterDataList != null && adapterDataList.size() > 0) {
                    LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(position,
                            Util.convertPhotoUrl(adapterDataList)));
                }
            }
        });
    }

    @Override
    public void getUserInfoData() {
        mUserDetailView.showLoading();
        mUserDetailView.meIsAuthor(UserPreference.isAnchor());
        ApiManager.getUserInfo(mUserDetailView.getUserId(), new IGetDataListener<UserDetailforOther>() {
            @Override
            public void onResult(UserDetailforOther userDetailforOther, boolean isEmpty) {
                if (userDetailforOther != null) {
                    UserDetail userDetail = userDetailforOther.getUserDetail();
                    if (userDetail != null) {
                        List<UserPhoto> userPhotos = userDetail.getUserPhotos();
                        if (userPhotos != null && userPhotos.size() > 0) {
                            mUserDetailView.canSeePhoto();
                            mAlbumPhotoAdapter.bind(userPhotos);
                            mUserDetailView.setViewPagerData(userPhotos);
                        }
                        int isSayHello = userDetail.getIsSayHello();
                        if (isSayHello == 1) {
                            mUserDetailView.isSayHello();
                        }
                        UserMend userMend = userDetail.getUserMend();
                        if (userMend != null) {
                            String isLogin = userMend.getIsLogin();
                            if (!TextUtils.isEmpty(isLogin) && isLogin.length() > 0) {
                                int status = Integer.parseInt(isLogin);
                                switch (status) {
                                    case 1:
                                        mUserDetailView.getStatus("在线");
                                        break;
                                    case -1:
                                        mUserDetailView.getStatus("不在线");
                                        break;
                                }

                            }
                            if (!TextUtils.isEmpty(userMend.getHeight())) {
                                mUserDetailView.setHeight(userMend.getHeight() + "cm");
                            } else {
                                mUserDetailView.setHeight("173cm");
                            }
                            if (!TextUtils.isEmpty(userMend.getIncomeId())) {
                                mUserDetailView.setIncome(DataPreference.getValueByKey(userMend.getIncomeId(), 2));
                            } else {
                                mUserDetailView.setIncome("保密");
                            }
                            if (!TextUtils.isEmpty(userMend.getRelationshipId()) && !userMend.getRelationshipId().equals("31")) {
                                mUserDetailView.setMerital_status(DataPreference.getValueByKey(userMend.getRelationshipId(), 1));
                            } else {
                                mUserDetailView.setMerital_status("保密");
                            }
                            if (!TextUtils.isEmpty(userMend.getEducationId())) {
                                mUserDetailView.setEducation(DataPreference.getValueByKey(userMend.getEducationId(), 0));
                            } else {
                                mUserDetailView.setEducation("保密");
                            }
                            if (!TextUtils.isEmpty(userMend.getOccupationId())) {
                                mUserDetailView.setJob(DataPreference.getValueByKey(userMend.getOccupationId(), 3));
                            } else {
                                mUserDetailView.setJob("保密");
                            }
                            if (!TextUtils.isEmpty(userMend.getWeight())) {
                                mUserDetailView.setWeight(userMend.getWeight() + "kg");
                            } else {
                                mUserDetailView.setWeight("保密");

                            }
                        }
                        HostInfo hostInfo = userDetail.getHostInfo();
                        if (hostInfo != null) {
                            int price = (int) hostInfo.getPrice();
                            mUserDetailView.setAnchorPrice(price);
                        } else {
                            mUserDetailView.setAnchorPrice(0);

                        }
                        mUserDetailView.getFollowOrUnFollow(userDetail);
                        UserBase userBase1 = userDetail.getUserBase();
                        if (userBase1 != null) {
                            userBase = userBase1;
                            mUserDetailView.setNickName(userBase1.getNickName());
                            mUserDetailView.setAge(userBase1.getAge() + "岁");
                            mUserDetailView.setBirthday(userBase1.getBirthday());
                            mUserDetailView.setSign(ParamsUtils.getSignValue(String.valueOf(userBase1.getSign())));
                            mUserDetailView.setUserId(userBase1.getAccount());
                            mUserDetailView.setAvatar(userBase1.getIconUrlMininum());

                            int userType = userBase1.getUserType();
                            if (userType == 1) {
                                mUserDetailView.isAnchor();
                                int gender = userBase1.getGender();
                                if (gender == 0){
                                    mUserDetailView.setSex("男");
                                }else{
                                    mUserDetailView.setSex("女");
                                }
                            } else {
                                int gender = userBase1.getGender();
                                if (gender == 0) {
                                    mUserDetailView.setSex("男");
                                    mUserDetailView.isMail(true);
                                } else {
                                    mUserDetailView.setSex("女");
                                    if (userBase1.getUserType() == 0) {
                                        mUserDetailView.isMail(false);
                                    }
                                }
                            }
                        }
                    }
                }
                mUserDetailView.dismissLoading();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (mUserDetailView != null) {
//                    mUserDetailView.dismissLoading();
                }
                if (isNetworkError) {
                    mUserDetailView.showNetworkError();
                } else {
//                    mUserDetailView.showTip(mContext.getString(R.string.load_fail));
                }
            }
        });
    }

    //关注
    @Override
    public void follow(String remoteUid) {
        ApiManager.follow(remoteUid, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                mUserDetailView.followSucceed();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (isNetworkError) {
                    mUserDetailView.showNetworkError();
                } else {
                    //   mUserDetailView.showTip(msg);
                }
            }
        });
    }

    //取消关注
    @Override
    public void unFollow(String remoteUid) {
        ApiManager.unFollow(remoteUid, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                mUserDetailView.followSucceed();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (isNetworkError) {
                    mUserDetailView.showNetworkError();
                } else {
                    mUserDetailView.showTip(msg);
                }
            }
        });
    }


    @Override
    public void goVideoCallPage() {
        if (userBase != null) {
            if (UserPreference.isAnchor() && userBase.getUserType() == 1) {
//                mUserDetailView.innerAnchor(mContext.getString(R.string.user_detail_other_is_author));
                //跳转视频聊天的界面
                VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                        , userBase.getNickName(), userBase.getIconUrlMininum(), 0, 0), mContext, "1");
            } else {
                if (!TextUtils.isEmpty(userBase.getStatus())) {
                    //跳转视频聊天的界面
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                            , userBase.getNickName(), userBase.getIconUrlMininum(), 0, 0), mContext, "1");
                }
            }
        }
    }

    @Override
    public void goWriteMessagePage() {
        //跳转写信的界面
        if (userBase != null) {
            if (UserPreference.isAnchor()) {
                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(userBase.getGuid(),
                        userBase.getAccount(), userBase.getNickName(), userBase.getIconUrlMininum(), 0));
            } else if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
                AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "聊天交友要通过真人认证哦", "",
                        "去认证", "取消", new OnDialogClickListener() {
                            @Override
                            public void onNegativeClick(View view) {
                            }

                            @Override
                            public void onPositiveClick(View view) {
                                LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                            }
                        }
                );
            } else {
                if (PayPreference.getDionmadsNum()>0 || UserPreference.isAnchor()) {
                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(userBase.getGuid(),
                            userBase.getAccount(), userBase.getNickName(), userBase.getIconUrlMininum(), 0));
                } else {
                    CustomDialogAboutPay.purchaseDiamondShow(mContext);
                }
            }
        }
    }

    @Override
    public void goToAvatarViewPager() {
        List<String> list_photo = new ArrayList<>();
        if (userBase != null) {
            list_photo.add(userBase.getIconUrlMiddle());
        }
        if (list_photo != null && list_photo.size() > 0) {
            // 点击图片查看大图
            LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(0,
                    list_photo));
        }
    }

    @Override
    public void sayHelloClick() {
        Toast.makeText(mContext, "打招呼成功", Toast.LENGTH_SHORT).show();
        mUserDetailView.isSayHello();
        if (userBase != null) {
            ApiManager.sayHello(String.valueOf(userBase.getGuid()), new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel videoStop, boolean isEmpty) {
                    EventBus.getDefault().post(new SayHelloEvent());
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {

                }
            });
        }
    }

    @Override
    public void sendVideoInvite(String remoteId) {
        if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
            AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "视频聊天需要通过真人认证哦", "",
                    "去认证", "取消", new OnDialogClickListener() {
                        @Override
                        public void onNegativeClick(View view) {
                        }

                        @Override
                        public void onPositiveClick(View view) {
                            LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                        }
                    }
            );

        } else {
            if (UserPreference.getStatus().equals("3")) {
                Toast.makeText(mContext, "您当前处于勿扰模式，请从设置中修改状态", Toast.LENGTH_SHORT).show();
            } else {
                if (userBase != null) {
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                            , userBase.getNickName(), userBase.getIconUrlMininum(), 0, 0), mContext, "1");
                }
            }
        }
    }

    @Override
    public void sendVoiceInvite(String remoteId) {//语音邀请
        if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
            AlertDialog.showNoCanceled(mUserDetailView.obtainFragmentManager(), "语音聊天需要通过真人认证哦", "",
                    "去认证", "取消", new OnDialogClickListener() {
                        @Override
                        public void onNegativeClick(View view) {
                        }

                        @Override
                        public void onPositiveClick(View view) {
                            LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                        }
                    }
            );
        } else {
            if (UserPreference.getStatus().equals("3")) {
                Toast.makeText(mContext, "您当前处于勿扰模式，请从设置中修改状态", Toast.LENGTH_SHORT).show();
            } else {
                if (userBase != null) {
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                            , userBase.getNickName(), userBase.getIconUrlMininum(), 1, 0), mContext, "1");
                }
            }
        }
    }
}
