package com.wiscomwis.jimo.ui.detail;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseAppCompatActivity;
import com.wiscomwis.jimo.common.CustomDialogAboutOther;
import com.wiscomwis.jimo.common.CustomDialogAboutPay;
import com.wiscomwis.jimo.common.TimeUtils;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.data.model.NettyMessage;
import com.wiscomwis.jimo.data.model.UserDetail;
import com.wiscomwis.jimo.data.model.UserPhoto;
import com.wiscomwis.jimo.data.preference.SwitchPreference;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.event.IsFollow;
import com.wiscomwis.jimo.event.SingleLoginFinishEvent;
import com.wiscomwis.jimo.parcelable.UserDetailParcelable;
import com.wiscomwis.jimo.ui.detail.adapter.UserAlbumPhotoAdapter;
import com.wiscomwis.jimo.ui.detail.contract.UserDetailContract;
import com.wiscomwis.jimo.ui.detail.presenter.UserDetailPresenter;
import com.wiscomwis.library.dialog.AlertDialog;
import com.wiscomwis.library.dialog.LoadingDialog;
import com.wiscomwis.library.dialog.OnDialogClickListener;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.SnackBarUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;

/**
 * 对面空间页面
 * Created by zhangdroid on 2017/5/27.
 */
public class UserDetailActivity extends BaseAppCompatActivity implements View.OnClickListener, UserDetailContract.IView {
    @BindView(R.id.user_detail_root_layout)
    RelativeLayout mLlRoot;
    @BindView(R.id.user_detail_rl_age_sex)
    RelativeLayout rl_sex;
    @BindView(R.id.user_detail_tv_nickname)
    TextView tv_nickname;
    @BindView(R.id.user_detail_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.user_detail_iv_avatar)
    ImageView iv_avatar;
    @BindView(R.id.user_detail_cardview_avatar)
    CardView cardView_avatar;
    @BindView(R.id.user_detail_tv_id)
    TextView tv_id;
    @BindView(R.id.user_detail_tv_age)
    TextView tv_age;
    @BindView(R.id.user_detail_tv_height)
    TextView tv_height;
    @BindView(R.id.user_detail_tv_distance)
    TextView tv_distance;
    @BindView(R.id.user_detail_tv_state)
    TextView tv_state;
    @BindView(R.id.user_detail_rl_follow)
    RelativeLayout rl_follow;
    @BindView(R.id.user_detail_tv_follow_num)
    TextView tv_follow_num;
    @BindView(R.id.user_detail_iv_recyclerview)
    RecyclerView photo_recyclerview;
    //基本信息
    @BindView(R.id.user_detail_base_tv_nickname)
    TextView tv_base_nickname;
    @BindView(R.id.user_detail_base_tv_sex)
    TextView tv_base_sex;
    @BindView(R.id.user_detail_base_tv_age)
    TextView tv_base_age;
    @BindView(R.id.user_detail_base_tv_apartment)
    TextView tv_base_apartment;
    @BindView(R.id.user_detail_base_tv_height)
    TextView tv_base_height;
    @BindView(R.id.user_detail_base_tv_income)
    TextView tv_base_income;
    @BindView(R.id.user_detail_base_tv_marital_status)
    TextView tv_base_marital_status;
    //详细资料
    @BindView(R.id.user_detail_detail_tv_education)
    TextView tv_detail_education;
    @BindView(R.id.user_detail_detail_tv_job)
    TextView tv_detail_job;
    @BindView(R.id.user_detail_detail_tv_birthday)
    TextView tv_detail_birthday;
    @BindView(R.id.user_detail_detail_tv_weight)
    TextView tv_detail_weight;
    @BindView(R.id.user_detail_detail_tv_sign)
    TextView tv_detail_sign;
    @BindView(R.id.user_detail_ll_send_video)
    LinearLayout ll_send_video;
    @BindView(R.id.user_detail_ll_say_hello)
    LinearLayout ll_sayHello;
    @BindView(R.id.user_detail_rl_is_follow)
    RelativeLayout rl_isfollow;
    @BindView(R.id.user_detail_ll_send_msg)
    LinearLayout ll_send_msg;
    @BindView(R.id.user_detail_toobar_rl)
    RelativeLayout rl_toobar;
    @BindView(R.id.user_detail_rl_bg)
    RelativeLayout rl_bg;
    @BindView(R.id.user_detail_detail_rl_talk_bg)
    RelativeLayout rl_talk_bg;
    @BindView(R.id.user_detail_detail_rl_author_talk)
    RelativeLayout rl_talk;
    @BindView(R.id.user_detail_detail_tv_how_money_dionamds)
    TextView tv_dionmads;
    @BindView(R.id.user_detail_tv_send_video)
    TextView tv_send_video;
    @BindView(R.id.user_detail_rl_set)
    RelativeLayout rl_set;
    @BindView(R.id.user_detail_iv_say_hello)
    ImageView iv_say_hello;
    @BindView(R.id.user_detail_iv_send_gifts)
    ImageView iv_send_gifts;
    @BindView(R.id.user_detail_tv_say_hello)
    TextView tv_say_hello;
    @BindView(R.id.user_detail_ll_send_voice)
    LinearLayout ll_send_voice;
    @BindView(R.id.user_detail_rl_photo)
    RelativeLayout rl_photo;
    @BindView(R.id.user_detail_rl_view_gray)
    RelativeLayout rl_view_gray;
    private boolean isSayHello = true;
    private String userAccount;
    private String userIcon;
    private String userNickName;
    private UserDetailPresenter userDetailPresenter;
    private UserDetailParcelable remoteUidParcelable;
    private boolean isFollow = true;
    private long lastClickTime = 0;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_user_detail;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        remoteUidParcelable = (UserDetailParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return mLlRoot;
    }

    @Override
    protected void initViews() {
        iv_say_hello.setBackgroundResource(R.drawable.userdetail_say_hello);
        userDetailPresenter = new UserDetailPresenter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        photo_recyclerview.setLayoutManager(linearLayoutManager);
        userDetailPresenter.start();
        if(TimeUtils.timeIsPast()){//判断页脚是否过期
            msgHandler.sendEmptyMessage(1);
        }else if(!TimeUtils.timeIsPast()){
            msgHandler.sendEmptyMessage(2);
        }
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
        cardView_avatar.setOnClickListener(this);
        rl_follow.setOnClickListener(this);
        ll_send_video.setOnClickListener(this);
        ll_sayHello.setOnClickListener(this);
        rl_isfollow.setOnClickListener(this);
        ll_send_msg.setOnClickListener(this);
        rl_set.setOnClickListener(this);
        iv_send_gifts.setOnClickListener(this);
        ll_send_voice.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        userDetailPresenter.getUserInfoData();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_detail_rl_back:
                finish();
                break;
            case R.id.user_detail_rl_set:
                CustomDialogAboutPay.reportShow(UserDetailActivity.this, remoteUidParcelable.remoteUid);

                break;
            case R.id.user_detail_cardview_avatar:
                userDetailPresenter.goToAvatarViewPager();
                break;
            case R.id.user_detail_rl_is_follow:
                if (isFollow) {
                    //关注
                    userDetailPresenter.follow(remoteUidParcelable.remoteUid);
                    setUnFollow();
                } else if (!isFollow) {
                    userDetailPresenter.unFollow(remoteUidParcelable.remoteUid);
                    setFollow();
                }
                break;
            case R.id.user_detail_ll_send_video:
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    userDetailPresenter.sendVideoInvite(remoteUidParcelable.remoteUid);
                }
                break;
            case R.id.user_detail_ll_say_hello:
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    if (isSayHello) {
                        userDetailPresenter.sayHelloClick();
                    }
                }
                break;
            case R.id.user_detail_ll_send_msg:
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    userDetailPresenter.goWriteMessagePage();
                }
                break;
            case R.id.user_detail_iv_send_gifts:
                CustomDialogAboutOther.giveGiftShow(UserDetailActivity.this, remoteUidParcelable.remoteUid, userAccount, userIcon, userNickName, 1,false);
                break;
            case R.id.user_detail_ll_send_voice:
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    userDetailPresenter.sendVoiceInvite(remoteUidParcelable.remoteUid);
                }
                break;
        }
    }

    //取消关注
    private void setUnFollow() {
        isFollow = false;
        rl_isfollow.setBackgroundResource(R.drawable.icon_followed);
    }

    //关注
    private void setFollow() {
        isFollow = true;
        rl_isfollow.setBackgroundResource(R.drawable.icon_unfollow);
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public String getUserId() {
        String remoteUid = null;
        if (remoteUidParcelable != null) {
            remoteUid = remoteUidParcelable.remoteUid;
        }
        if (!TextUtils.isEmpty(remoteUid)) {
            return remoteUid;
        } else {
            return null;
        }
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void followSucceed() {
        //关注成功，或者取消关注
        if (!isFollow) {
            Toast.makeText(UserDetailActivity.this, getString(R.string.user_detail_follow_success), Toast.LENGTH_SHORT).show();
        } else if (isFollow) {
            Toast.makeText(UserDetailActivity.this, getString(R.string.user_detail_follow_cancle), Toast.LENGTH_SHORT).show();
        }
        EventBus.getDefault().post(new IsFollow());
    }

    @Override
    public void setViewPagerData(final List<UserPhoto> userPhotos) {

    }

    // 获取是否关注的信息
    @Override
    public void getFollowOrUnFollow(UserDetail userDetail) {
        if (userDetail != null) {
            if (userDetail.getIsFollow().equals("1")) {
                //已经关注
                setUnFollow();
            } else if (userDetail.getIsFollow().equals("0")) {
                //没有关注
                setFollow();
            }
        }
    }

    @Override
    public void getStatus(String status) {
        tv_state.setText(status);
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void isAnchor() {
        if (SwitchPreference.getAlipay() == 1) {
            rl_talk.setVisibility(View.VISIBLE);
        } else {
            rl_talk.setVisibility(View.GONE);
        }
        rl_talk_bg.setVisibility(View.VISIBLE);
    }

    @Override
    public void innerAnchor(String tip) {
        AlertDialog.show(getSupportFragmentManager(), mContext.getString(R.string.alert), tip,
                mContext.getString(R.string.setting_true), mContext.getString(R.string.video_cancel), new OnDialogClickListener() {
                    @Override
                    public void onNegativeClick(View view) {
                    }

                    @Override
                    public void onPositiveClick(View view) {
                    }
                });
    }

    @Override
    public void setAnchorPrice(int price) {
        tv_dionmads.setText(price + mContext.getString(R.string.invite_video_unit));
    }

    @Override
    public void setNickName(String nickName) {
        userNickName = nickName;
        tv_base_nickname.setText(nickName);
        tv_nickname.setText(nickName);
    }

    @Override
    public void setSex(String sex) {
        tv_base_sex.setText(sex);
        if (sex.equals("男")){
            rl_sex.setBackgroundResource(R.drawable.icon_male);
        }else{
            rl_sex.setBackgroundResource(R.drawable.icon_famle);
        }

    }

    @Override
    public void setAge(String age) {
        tv_age.setText(age);
        tv_base_age.setText(age);
    }

    @Override
    public void setApartment(String apartment) {
        tv_base_apartment.setText(apartment);
    }

    @Override
    public void setHeight(String height) {
        tv_base_height.setText(height);
        tv_height.setText(height);
    }

    @Override
    public void setIncome(String income) {
        tv_base_income.setText(income);
    }

    @Override
    public void setMerital_status(String status) {
        tv_base_marital_status.setText(status);
    }

    @Override
    public void setEducation(String education) {
        tv_detail_education.setText(education);
    }

    @Override
    public void setJob(String job) {
        tv_detail_job.setText(job);
    }

    @Override
    public void setBirthday(String birthday) {
        tv_detail_birthday.setText(birthday);
    }

    @Override
    public void setWeight(String weight) {
        tv_detail_weight.setText(weight);
    }

    @Override
    public void setSign(String sign) {
        tv_detail_sign.setText(sign);
    }

    @Override
    public void setAvatar(String avatar) {
        userIcon = avatar;
        ImageLoaderUtil.getInstance().loadImage(getApplicationContext(), new ImageLoader.Builder().url(avatar)
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_avatar).build());
    }

    @Override
    public void setUserId(String userId) {
        userAccount = userId;
        tv_id.setText("ID: " + userId);
    }

    @Override
    public void setAdapter(UserAlbumPhotoAdapter mAlbumPhotoAdapter) {
        photo_recyclerview.setAdapter(mAlbumPhotoAdapter);
    }

    @Override
    public void getOrderInfo(String info) {
    }

    @Override
    public void isMail(boolean flag) {
        if (flag) {
            rl_bg.setBackgroundResource(R.drawable.icon_male_background);
            rl_toobar.setBackgroundResource(R.drawable.icon_male_background);
        } else {
            if (UserPreference.isMale()) {
                ll_send_video.setVisibility(View.GONE);
                ll_send_voice.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void videoInvite(String video) {
        tv_send_video.setText(video);
    }

    @Override
    public void meIsAuthor(boolean flag) {
        tv_send_video.setText(mContext.getString(R.string.video_video));
        if (flag) {
            iv_send_gifts.setVisibility(View.GONE);
        }
    }

    @Override
    public void isSayHello() {
        isSayHello = false;
        iv_say_hello.setBackgroundResource(R.drawable.userdetail_say_helloed);
        tv_say_hello.setText(mContext.getString(R.string.helloed));
    }

    @Override
    public void canSeePhoto() {
        rl_photo.setVisibility(View.VISIBLE);
        rl_view_gray.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }

    @Subscribe
    public void onEvent(NettyMessage event) {
        msgHandler.sendEmptyMessage(1);
    }
    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    closeShowPop();
                    break;
            }
        }
    };

    @Override
    protected void closeShowPop() {
        super.closeShowPop();
    }

    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }
}
