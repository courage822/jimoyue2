package com.wiscomwis.jimo.ui.detail.contract;

import android.support.v4.app.FragmentManager;

import com.wiscomwis.jimo.mvp.BasePresenter;
import com.wiscomwis.jimo.mvp.BaseView;
import com.wiscomwis.jimo.ui.detail.adapter.AlbumPhotoAdapter;

import java.io.File;

/**
 * Created by Administrator on 2017/6/9.
 */
public interface EditInfoContract {
    interface IView extends BaseView {
        void showLoading();

        void dismissLoading();

        void showNetworkError();

        String getIntroducation();

        void setUserAvator(String photoUrl,String status);
        void setIntroducation(String introducation);

        FragmentManager obtainFragmentManager();

        void finishActivity();
        String stringNickName();
        String stringHeight();
        String stringIncome();
        String stringRelation();
        String stringEducation();
        String stringJob();
        String stringBirthday();
        String stringWeight();
        String stringApartment();
        String stringSign();
        String stringAge();
        void isMale(boolean male);
        void getNickNmae(String name);
        void getUserId(String id);
        void getAge(String age);
        void getHeight(String height);
        void getApartment(String apartment);
        void getEducation(String education);
        void getIncome(String income);
        void getMarriage(String marriage);
        void getJob(String job);
        void getBirthday(String birthday);
        void getWeight(String weight);
        void getSign(String sign);
        void setAdapter(AlbumPhotoAdapter adapter);
        void setGoneImageView();
        void setStatus(String stauts);

    }

    interface IPresenter extends BasePresenter {

        /**
         * 设置用户信息
         */
        void setLocalInfo();

        /**
         * 上传用户信息
         */
        void upLoadMyInfo();

        /**
         * 上传用户的头像
         */
        void upLoadAvator(File file, boolean photoType);

        /**
         * 获取用户的个人信息
         */
        void getUserInfo();

        /**
         * 获取当前的年份和月份
         */
        void getYearsAndMonth(String year,String month);
    }
}
