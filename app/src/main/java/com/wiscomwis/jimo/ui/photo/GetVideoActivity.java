package com.wiscomwis.jimo.ui.photo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiscomwis.jimo.R;
import com.wiscomwis.library.util.FileUtil;
import com.wiscomwis.library.util.ToastUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 录像/从本地选择视频
 * Created by xu on 2017/11/29.
 */
public class GetVideoActivity extends Activity {
    @BindView(R.id.photo_root)
    LinearLayout mLlRoot;
    @BindView(R.id.choose_from_album)
    TextView mTvChooseFromAlbum;
    @BindView(R.id.take_photo)
    TextView mTvTakePhoto;
    @BindView(R.id.tv_type)
    TextView tv_type;

    private static final int PERMISSION_CODE_READ_EXTERNAL_STORAGE = 0;
    public static final int RECORD_SYSTEM_VIDEO = 1;
    private static final int REQUEST_CODE_TAKE_VIDEO = 0;
    private static OnGetVideoListener sOnGetVideoListener;
    private static OnGetVideoPathListener sOnGetVideoPathListener;

    public interface OnGetVideoListener {

        /**
         * 返回选择后的视频文件
         *
         * @param file     包含视频的File对象
         * @param duration 视频的时长
         */
        void getSelectedVideo(File file, String duration, Bitmap bitmap);
    }

    public interface OnGetVideoPathListener {

        /**
         * 返回选择后的视频路径
         *
         * @param path     视频路径
         * @param duration 视频的时长
         */
        void getSelectedVideoPath(String path, String duration);
    }

    // 视频临时保存路径
    private String mTempVideoPath;
    private static boolean mIsShowUi = true;
    private static final String INTENT_KEY_IS_TAKE_PHOTO = "is_take_video";

    public static void toGetVideoActivity(Context context, boolean isTakePhoto, OnGetVideoPathListener listener) {
        sOnGetVideoPathListener = listener;
        mIsShowUi = false;
        Intent intent = new Intent(context, GetVideoActivity.class);
        intent.putExtra(INTENT_KEY_IS_TAKE_PHOTO, isTakePhoto);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 获取本地视频（拍照/从相册选取）
     *
     * @param context  上下文
     * @param listener 图片监听器
     */
    public static void toGetVideoActivity(Context context, OnGetVideoListener listener) {
        sOnGetVideoListener = listener;
        mIsShowUi = true;
        Intent intent = new Intent(context, GetVideoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_photo);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this);
        tv_type.setText(getString(R.string.upload_video));
        mTvChooseFromAlbum.setText(getString(R.string.video_video));
        mTvTakePhoto.setText(getString(R.string.video_shoot));

        // 初始化图片保存路径
        mTempVideoPath = FileUtil.getExternalFilesDir(this, Environment.DIRECTORY_DCIM)
                + File.separator + ".wmv";
        addListener();
        if (!mIsShowUi) {
            mLlRoot.setVisibility(View.GONE);
            Bundle bundle = getIntent().getExtras();
            if (null != bundle) {
                if (bundle.getBoolean(INTENT_KEY_IS_TAKE_PHOTO)) {
                    mTvTakePhoto.performClick();
                } else {
                    mTvChooseFromAlbum.performClick();
                }
            }
        }
    }

    protected void addListener() {
        //从本地获取视频
        mTvChooseFromAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 隐藏对话框
                mLlRoot.setVisibility(View.GONE);
                // 从本地获取视频
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_CODE_TAKE_VIDEO);

            }
        });
        //录制视频
        mTvTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 隐藏对话框
                mLlRoot.setVisibility(View.GONE);
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 300); //限制的录制时长 以秒为单位
                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0); //设置拍摄的质量最小是0，最大是1（建议不要设置中间值，不同手机似乎效果不同。。。）
                intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 50 * 1024 * 1024);//限制视频文件大小 以字节为单位
                startActivityForResult(intent, RECORD_SYSTEM_VIDEO);
            }
        });
    }

    private File getOutputMediaFile() {
        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            ToastUtil.showShortToast(this, getString(R.string.check_sd));
            return null;
        }

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory
                (Environment.DIRECTORY_DCIM), "MyCameraApp");
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        mTempVideoPath = mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".wmv";
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".wmv");
        return mediaFile;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CODE_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                takePhoto();
            } else {
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_TAKE_VIDEO:// 获取视频
                    if (data != null) {
                        Uri uri = data.getData();
                        if (uri != null) {
                            Cursor cursor = getContentResolver().query(uri, null, null,
                                    null, MediaStore.Video.Media.DEFAULT_SORT_ORDER);
                            if (cursor != null) {
                                if (cursor.moveToFirst()) {
                                    String[] filePathColumn = {MediaStore.Video.Media.DATA};
                                    String[] fileDuration = {MediaStore.Video.Media.DURATION};

                                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                    String v_path = cursor.getString(columnIndex);
                                    Log.e("v_path", v_path);

                                    String duration = null;
                                    try {
                                        duration =cursor.getString(cursor.getColumnIndex(fileDuration[0]));

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    String v_duration = TextUtils.isEmpty(duration) ? "10" :(Math.round( Integer.parseInt(duration)/1000) + "");
                                    Log.e("v_duration", v_duration);
                                    cursor.close();
                                    File file = new File(v_path);
                                    if (file.exists()) {
                                        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                                        mmr.setDataSource(v_path);
                                        Bitmap bitmap = mmr.getFrameAtTime();
                                        sOnGetVideoListener.getSelectedVideo(file, v_duration, bitmap);
                                        mmr.release();
                                    }
                                }
                            }
                            cursor.close();
                        }
                    }
                    finish();
                    break;
                case RECORD_SYSTEM_VIDEO://录制视频
                    if (null != data) {
                        Uri uri = data.getData();
                        if (uri == null) {
                            return;
                        } else {
                            Cursor cursor = getContentResolver().query(uri,
                                    new String[]{MediaStore.MediaColumns.DATA, MediaStore.Video.Media.DURATION},
                                    null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                String[] filePathColumn = {MediaStore.Video.Media.DATA,};
                                String[] fileDuration = {MediaStore.Video.Media.DURATION};

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String v_path = cursor.getString(columnIndex);
                                Log.e("v_path_record", v_path);

                                String duration = null;
                                try {
                                    duration =cursor.getString(cursor.getColumnIndex(fileDuration[0]));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                String v_duration = TextUtils.isEmpty(duration) ? "10" : duration;
                                Log.e("record_duration", v_duration);

                                cursor.close();
                                File file = new File(v_path);

                                if (file.exists()) {
                                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                                    mmr.setDataSource(v_path);
                                    Bitmap bitmap = mmr.getFrameAtTime();
                                    sOnGetVideoListener.getSelectedVideo(file, v_duration, bitmap);
                                    mmr.release();
                                }
                            }
                        }
                    }
                    finish();
                    break;
            }
        }else{
            finish();
        }
    }
}
