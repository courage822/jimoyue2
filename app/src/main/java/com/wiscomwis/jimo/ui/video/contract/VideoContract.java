package com.wiscomwis.jimo.ui.video.contract;

import android.widget.FrameLayout;

import com.wiscomwis.jimo.data.model.VideoMsg;
import com.wiscomwis.jimo.event.AgoraEvent;
import com.wiscomwis.jimo.event.AgoraMediaEvent;
import com.wiscomwis.jimo.mvp.BasePresenter;
import com.wiscomwis.jimo.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/6/13.
 */
public interface VideoContract {
    interface IView extends BaseView {

        void finishActivity();

        /**
         * 延时执行
         *
         * @param runnable
         * @param delayedSecs
         */
        void postDelayed(Runnable runnable, int delayedSecs);

        void showNetworkError();

        /**
         * 返回添加本地视频容器
         */
        FrameLayout getLocalVideoView();

        /**
         * 返回添加远端视频容器
         */
        FrameLayout getRemoteVideoView();

        /**
         * 视频计时
         */
        void videoTimeing();

        /**
         * 是否显示关注按钮
         *
         * @param isVisible true可见，fasle不可见
         */
        void setFollowVisibility(boolean isVisible);

        /**
         * @return 返回输入的信息
         */
        String getInputMessage();

        /**
         *
         */
        String getVideoTime();
        /**
         * 清空输入的信息
         */
        void clearInput();

        /**
         * 更新消息列表（新消息）
         */
        void updateMsgList(VideoMsg videoMsg);
        /**
         * 发送礼物开始动画
         */
        void startAnimation();
    }

    interface IPresenter extends BasePresenter {

        /**
         * 处理视频相关事件
         *
         * @param event {@link AgoraMediaEvent}
         */
        void handleVideoEvent(AgoraMediaEvent event);

        /**
         * 切换摄像头
         */
        void switchCamera();

        /**
         * 关闭视频
         */
        void closeVideo();

        /**
         * 发送文字消息
         *
         * @param account 接收方account
         */
        void sendMsg(String account);

        /**
         * 处理信令相关事件
         *
         * @param event {@link AgoraEvent}
         */
        void handleAgoraEvent(AgoraEvent event);

        /**
         * 将计时秒数转换成时间
         */
        String convertSecondsToString(int seconds);
        void sendDurationMsg();
    }
}
