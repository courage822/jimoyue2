package com.wiscomwis.jimo.ui.register.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.ChangePassword;
import com.wiscomwis.jimo.data.model.MyInfo;
import com.wiscomwis.jimo.data.model.UserBase;
import com.wiscomwis.jimo.data.model.UserDetail;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.ui.register.contract.ChangePasswordContract;


/**
 * Created by zhangdroid on 2017/5/31.
 */
public class ChangePasswordPresenter implements ChangePasswordContract.IPresenter {
    private ChangePasswordContract.IView mChangePasswordView;
    private Context context;

    public ChangePasswordPresenter(ChangePasswordContract.IView mChangePasswordView) {
        this.mChangePasswordView = mChangePasswordView;
        this.context = mChangePasswordView.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void changePassword() {
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        UserBase userBase = userDetail.getUserBase();
                        if (userBase != null) {
                            mChangePasswordView.getId(userBase.getAccount());
                            mChangePasswordView.getOriginalPassword(userBase.getPassword());
                            UserPreference.saveUserInfo(userBase);
                        }

                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    @Override
    public void sureConfirm() {
        String newPassword = mChangePasswordView.getNewPassword();
        if (!TextUtils.isEmpty(newPassword)) {
            String confirmPassrod = mChangePasswordView.confirmPassrod();
            if (!TextUtils.isEmpty(confirmPassrod)) {

                ApiManager.changePassword(UserPreference.getPassword(), newPassword, confirmPassrod, new IGetDataListener<ChangePassword>() {
                    @Override
                    public void onResult(ChangePassword changePassword, boolean isEmpty) {
                        if (changePassword != null) {
                            mChangePasswordView.showTip(changePassword.getMsg());
                            mChangePasswordView.finishActivity();
                        }
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                        mChangePasswordView.showTip(msg);
                    }
                });
            } else {
                mChangePasswordView.showTip("确认密码不能为空");
            }
        } else {
            mChangePasswordView.showTip("新密码不能为空");
        }

    }
}
