package com.wiscomwis.jimo.ui.pay.activity;

import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseFragmentActivity;
import com.wiscomwis.library.net.NetUtil;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/9/13.
 */

public class WithdrawExplainActivity extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.withdrawexplain_activity_iv_back)
    ImageView iv_back;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_withdrawexplain;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void setListeners() {
        iv_back.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.withdrawexplain_activity_iv_back:
                finish();
                break;
        }
    }
}
