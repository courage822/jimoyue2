package com.wiscomwis.jimo.ui.detail.contract;

import android.support.v4.app.FragmentManager;

import com.wiscomwis.jimo.mvp.BasePresenter;
import com.wiscomwis.jimo.mvp.BaseView;
import com.wiscomwis.jimo.ui.detail.adapter.AlbumAdapter;

/**
 * Created by Administrator on 2017/6/14.
 */
public interface AlbumContract {


    interface IView extends BaseView {
        void showLoading();

        void dismissLoading();

        void showNetworkError();

        FragmentManager obtainFragmentManager();

        void setAdapter(AlbumAdapter adater);
    }

    interface IPresenter extends BasePresenter {

        /**
         * 获取图片列表
         */
        void getPhotoInfo();

        /**
         * 上传图片
         */
        void uploadImage();
        /**
         * 删除照片
         */
        void updateImage(String picId, int positon);
    }

}
