package com.wiscomwis.jimo.ui.chat;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMVoiceMessageBody;
import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.HyphenateHelper;
import com.wiscomwis.jimo.common.RecordUtil;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.event.SeeDetailEvent;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.adapter.provider.ItemViewProvider;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.DateTimeUtil;
import com.wiscomwis.library.util.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * 聊天语音消息（接收方）
 * Created by zhangdroid on 2017/6/29.
 */
public class VoiceMessageLeftProvider implements ItemViewProvider<EMMessage> {
    private Context mContext;
    private String mAvatarUrl;
    private List<EMMessage> messageList;


    public VoiceMessageLeftProvider(Context context, String url, List<EMMessage> messageList) {
        this.mContext = context;
        this.mAvatarUrl = url;
        this.messageList = messageList;
    }

    @Override
    public int getItemViewLayoutResId() {
        return R.layout.item_chat_voice_left;
    }

    @Override
    public boolean isViewType(EMMessage item, int position) {
        return (item.getType() == EMMessage.Type.VOICE && item.direct() == EMMessage.Direct.RECEIVE);
    }

    @Override
    public void convert(final EMMessage emMessage, int position, final RecyclerViewHolder holder) {
        // 头像
        ImageView ivAvatar = (ImageView) holder.getView(R.id.item_chat_voice_avatar_left);
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                .error(Util.getDefaultImageCircle()).url(mAvatarUrl).imageView(ivAvatar).build());
        ivAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 发送事件，查看用户详情
                EventBus.getDefault().post(new SeeDetailEvent());
            }
        });
        if (null != emMessage) {
            if (position > 0) {
                long msgTime = messageList.get(position - 1).getMsgTime();
                if ((emMessage.getMsgTime()-msgTime) / (1000 * 60) > 5) {
                    holder.getView(R.id.item_chat_voice_time_left).setVisibility(View.VISIBLE);
                    holder.setText(R.id.item_chat_voice_time_left, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
                } else {
                    holder.getView(R.id.item_chat_voice_time_left).setVisibility(View.GONE);
                }
            } else {
                holder.getView(R.id.item_chat_voice_time_left).setVisibility(View.VISIBLE);
                holder.setText(R.id.item_chat_voice_time_left, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
            }

            final EMVoiceMessageBody emVoiceMessageBody = (EMVoiceMessageBody) emMessage.getBody();
            if (null != emVoiceMessageBody) {
                // 如果语音文件本地路径不存在，则下载
                if (TextUtils.isEmpty(emVoiceMessageBody.getLocalUrl())) {
                    HyphenateHelper.getInstance().downloadAttachment(emMessage);
                }
                // 语音时间
                holder.setText(R.id.item_chat_voice_left_duration, TextUtils.concat(String.valueOf(emVoiceMessageBody.getLength()), "'").toString());
                holder.setOnClickListener(R.id.item_chat_voice_left_ll, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 播放语音动画
                        List<Drawable> list = new ArrayList<Drawable>();
                        list.add(mContext.getDrawable(R.drawable.sound_wave_left1));
                        list.add(mContext.getDrawable(R.drawable.sound_wave_left2));
                        list.add(mContext.getDrawable(R.drawable.sound_wave_left3));
                        final AnimationDrawable animationDrawable = Utils.getFrameAnim(list, true, 200);
                        holder.setImageDrawable(R.id.item_chat_voice_left, animationDrawable);
                        animationDrawable.start();
                        if (RecordUtil.getInstance().isPlaying()) {
                            RecordUtil.getInstance().stop();
                        }
                        RecordUtil.getInstance().play(emVoiceMessageBody.getLocalUrl(), new RecordUtil.OnPlayerListener() {
                            @Override
                            public void onCompleted() {
                                animationDrawable.stop();
                                holder.setImageResource(R.id.item_chat_voice_left, R.drawable.sound_wave_left3);
                            }

                            @Override
                            public void onPaused() {
                                animationDrawable.stop();
                                holder.setImageResource(R.id.item_chat_voice_left, R.drawable.sound_wave_left3);
                            }
                        });
                    }
                });
            }
        }
    }

}
