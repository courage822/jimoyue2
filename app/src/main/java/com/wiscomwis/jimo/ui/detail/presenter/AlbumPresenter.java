package com.wiscomwis.jimo.ui.detail.presenter;

import android.content.Context;
import android.view.View;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.MyInfo;
import com.wiscomwis.jimo.data.model.UpLoadMyPhoto;
import com.wiscomwis.jimo.data.model.UserBase;
import com.wiscomwis.jimo.data.model.UserDetail;
import com.wiscomwis.jimo.data.model.UserPhoto;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.parcelable.BigPhotoParcelable;
import com.wiscomwis.jimo.ui.detail.adapter.AlbumAdapter;
import com.wiscomwis.jimo.ui.detail.contract.AlbumContract;
import com.wiscomwis.jimo.ui.detail.listener.OnItemClickListener;
import com.wiscomwis.jimo.ui.photo.BigPhotoActivity;
import com.wiscomwis.jimo.ui.photo.GetPhotoActivity;
import com.wiscomwis.library.dialog.LoadingDialog;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.ToastUtil;

import java.io.File;
import java.util.List;

/**
 * Created by Administrator on 2017/6/14.
 */
public class AlbumPresenter implements AlbumContract.IPresenter {
    private AlbumContract.IView mAlbumIview;
    private Context mContext;
    private AlbumAdapter mAlbumPhotoAdapter;

    public AlbumPresenter(AlbumContract.IView mAlbumIview) {
        this.mAlbumIview = mAlbumIview;
        this.mContext = mAlbumIview.obtainContext();
    }

    @Override
    public void start() {
        mAlbumPhotoAdapter = new AlbumAdapter(mContext);
        mAlbumIview.setAdapter(mAlbumPhotoAdapter);
        mAlbumPhotoAdapter.setAddClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (mAlbumPhotoAdapter.getItemCount() == 7) {
                    ToastUtil.showShortToast(mContext, mContext.getString(R.string.image_max));
                } else {
                    uploadImage();
                }
            }
        });
        mAlbumPhotoAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                //大图预览
                List<UserPhoto> photos = mAlbumPhotoAdapter.getPhotos();
                if (photos != null) {
                    // 点击图片查看大图
                    LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(position,
                            Util.convertPhotoUrl(photos)));
                }
            }
        });
        mAlbumPhotoAdapter.setOnLongClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                UserPhoto userPhoto = mAlbumPhotoAdapter.getUserPhoto(position);
                if (userPhoto != null)
                    updateImage(String.valueOf(userPhoto.getGuid()), position);
            }
        });
    }

    @Override
    public void getPhotoInfo() {
        mAlbumIview.showLoading();
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        List<UserPhoto> userPhotos = userDetail.getUserPhotos();
                        mAlbumIview.dismissLoading();
                        mAlbumPhotoAdapter.setUserPhoto(userPhotos);
                        UserBase userBase = userDetail.getUserBase();
                        if (userBase != null) {
                            UserPreference.saveUserInfo(userBase);
                        }
                    } else {
                        mAlbumIview.dismissLoading();
                    }
                } else {
                    mAlbumIview.dismissLoading();
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mAlbumIview.dismissLoading();
                if (isNetworkError) {
                    mAlbumIview.showNetworkError();
                } else {
                    mAlbumIview.showTip(msg);
                }
            }
        });
    }

    @Override
    public void uploadImage() {
        GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
            @Override
            public void getSelectedPhoto(File file) {
                if (null != file) {
                    LoadingDialog.show(mAlbumIview.obtainFragmentManager());
                    ApiManager.upLoadMyPhotoOrAvator(file, false, new IGetDataListener<UpLoadMyPhoto>() {
                        @Override
                        public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                            LoadingDialog.hide();
                            if (upLoadMyPhoto != null) {
                                final UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                                mAlbumPhotoAdapter.addUserPhoto(userPhoto);
                            }
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                            LoadingDialog.hide();
                            if (isNetworkError) {
                                mAlbumIview.showNetworkError();
                            } else {
                                mAlbumIview.showTip(msg);
                            }
                        }
                    });
                }
            }
        });
    }


    @Override
    public void updateImage(final String photoId, final int position) {
        GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
            @Override
            public void getSelectedPhoto(File file) {
                if (null != file) {
                    LoadingDialog.show(mAlbumIview.obtainFragmentManager());
                    ApiManager.updateUserPhotoes(file, false, photoId, new IGetDataListener<UpLoadMyPhoto>() {
                        @Override
                        public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                            LoadingDialog.hide();
                            if (upLoadMyPhoto != null) {
                                final UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                                mAlbumPhotoAdapter.updateUserPhoto(position, userPhoto);
                            }
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                            LoadingDialog.hide();
                            if (isNetworkError) {
                                mAlbumIview.showNetworkError();
                            } else {
                                mAlbumIview.showTip(msg);
                            }
                        }
                    });
                }
            }
        });
    }
}
