package com.wiscomwis.jimo.ui.detail;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseAppCompatActivity;
import com.wiscomwis.jimo.common.RingManage;
import com.wiscomwis.jimo.common.TimeUtils;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.data.constant.DataDictManager;
import com.wiscomwis.jimo.data.model.NettyMessage;
import com.wiscomwis.jimo.data.preference.DataPreference;
import com.wiscomwis.jimo.event.SingleLoginFinishEvent;
import com.wiscomwis.jimo.ui.detail.adapter.AlbumPhotoAdapter;
import com.wiscomwis.jimo.ui.detail.contract.EditInfoContract;
import com.wiscomwis.jimo.ui.detail.presenter.EditInfoPresenter;
import com.wiscomwis.jimo.ui.dialog.AgeSelectDialog;
import com.wiscomwis.jimo.ui.dialog.DialogUtil;
import com.wiscomwis.jimo.ui.dialog.OnDoubleDialogClickListener;
import com.wiscomwis.jimo.ui.dialog.OnEditDoubleDialogClickListener;
import com.wiscomwis.jimo.ui.dialog.OneWheelDialog;
import com.wiscomwis.jimo.ui.dialog.ThreeWheelDialog;
import com.wiscomwis.jimo.ui.photo.GetPhotoActivity;
import com.wiscomwis.library.dialog.LoadingDialog;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.SnackBarUtil;

import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import butterknife.BindView;

/**
 * 编辑个人信息页面
 * Created by zhangdroid on 2017/5/27.
 */
public class EditInfoActivity extends BaseAppCompatActivity implements View.OnClickListener, EditInfoContract.IView {
    @BindView(R.id.edit_info_root_layout)
    LinearLayout mLlRoot;//根布局
    @BindView(R.id.edit_info_rl_age_sex)
    RelativeLayout rl_sex;//根布局
    @BindView(R.id.edit_info_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.edit_info_cardview_avatar)
    CardView fl_upload_photo;//上传头像
    @BindView(R.id.edit_info_iv_avatar)
    ImageView iv_avatar;//头像
    @BindView(R.id.iv_avatar_status)
    ImageView iv_avatar_status;//头像状态
    @BindView(R.id.edit_info_tv_id)
    TextView tv_id;
    @BindView(R.id.edit_info_rl_album)
    RelativeLayout rl_album;//相册
    @BindView(R.id.edit_info_iv_recyclerview)
    RecyclerView recyclerView_album;//相册展示
    @BindView(R.id.edit_info_base_rl_nickname)
    RelativeLayout rl_nickname;
    @BindView(R.id.edit_info_base_tv_nickname)
    TextView et_nickname;//昵称
    @BindView(R.id.edit_info_base_rl_apartment)
    RelativeLayout rl_apartment;
    @BindView(R.id.edit_info_base_tv_apartment)
    TextView tv_apartment;//居住地
    @BindView(R.id.edit_info_base_rl_height)
    RelativeLayout rl_height;
    @BindView(R.id.edit_info_base_tv_height)
    TextView tv_height;//身高
    @BindView(R.id.edit_info_base_rl_income)
    RelativeLayout rl_income;
    @BindView(R.id.edit_info_base_tv_income)
    TextView tv_income;//收入
    @BindView(R.id.edit_info_base_rl_marital_status)
    RelativeLayout rl_marital_status;
    @BindView(R.id.edit_info_base_tv_marital_status)
    TextView tv_marital_status;//婚姻状况
    @BindView(R.id.edit_info_rl_education)
    RelativeLayout rl_eduction;
    @BindView(R.id.edit_info_detail_tv_education)
    TextView tv_education;//学历
    @BindView(R.id.edit_info_rl_job)
    RelativeLayout rl_job;
    @BindView(R.id.edit_info_detail_tv_job)
    TextView tv_job;//工作
    @BindView(R.id.edit_info_rl_birthday)
    RelativeLayout rl_birthday;
    @BindView(R.id.edit_info_detail_tv_birthday)
    TextView tv_birthday;//生日
    @BindView(R.id.edit_info_rl_weight)
    RelativeLayout rl_weight;
    @BindView(R.id.edit_info_detail_tv_weight)
    TextView tv_weight;
    @BindView(R.id.edit_info_rl_sign)
    RelativeLayout rl_sign;
    @BindView(R.id.edit_info_detail_tv_sign)
    TextView tv_sign;
    @BindView(R.id.edit_info_rl_toobar)
    RelativeLayout rl_toobar;
    @BindView(R.id.edit_info_rl_bg)
    RelativeLayout rl_bg;
    @BindView(R.id.edit_info_tv_nickname)
    TextView tv_nickname;
    @BindView(R.id.edit_info_tv_age)
    TextView tv_age;
    @BindView(R.id.edit_info_base_tv_sex)
    TextView tv_sex;
    @BindView(R.id.edit_info_base_tv_age)
    TextView tv_age2;
    @BindView(R.id.edit_info_tv_height)
    TextView tv_height2;
    @BindView(R.id.edit_info_tv_status)
    TextView tv_status;
    @BindView(R.id.edit_info_base_rl_age)
    RelativeLayout rl_age;
    private EditInfoPresenter mEditInfoPresenter;
    InputMethodManager imm;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_edit_info;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected View getNoticeView() {
        return mLlRoot;
    }

    @Override
    protected void initViews() {
        mEditInfoPresenter = new EditInfoPresenter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        recyclerView_album.setLayoutManager(linearLayoutManager);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mEditInfoPresenter.getUserInfo();
        if (TimeUtils.timeIsPast()) {//判断页脚是否过期
            msgHandler.sendEmptyMessage(1);
        } else if (!TimeUtils.timeIsPast()) {
            msgHandler.sendEmptyMessage(2);
        }
    }

    @Override
    protected void setListeners() {
        fl_upload_photo.setOnClickListener(this);
        rl_album.setOnClickListener(this);
        rl_nickname.setOnClickListener(this);
        rl_apartment.setOnClickListener(this);
        rl_height.setOnClickListener(this);
        rl_income.setOnClickListener(this);
        rl_marital_status.setOnClickListener(this);
        rl_eduction.setOnClickListener(this);
        rl_job.setOnClickListener(this);
        rl_birthday.setOnClickListener(this);
        rl_weight.setOnClickListener(this);
        rl_sign.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_age.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        mEditInfoPresenter.setLocalInfo();

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        mEditInfoPresenter.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_info_cardview_avatar://头像
                GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        mEditInfoPresenter.upLoadAvator(file, true);
                    }
                });
                break;
            case R.id.edit_info_rl_back:
                mEditInfoPresenter.upLoadMyInfo();
                break;
            case R.id.edit_info_rl_album:// 跳转到相册页面
                LaunchHelper.getInstance().launch(mContext, AlbumActivity.class);
                break;
            case R.id.edit_info_base_rl_nickname:// 昵称
                DialogUtil.showEditDoubleBtnDialog(getSupportFragmentManager(), mContext.getString(R.string.register_name), mContext.getString(R.string.video_input_hint), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), true, new OnEditDoubleDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String content) {
                        if (!TextUtils.isEmpty(content.trim())) {
                            et_nickname.setText(content);
                            tv_nickname.setText(content);
                        }
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_base_rl_apartment:// 居住地
//                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), DataDictManager.getStatteList(),"居住地","确定","取消", true, new OneWheelDialog.OnOneWheelDialogClickListener() {
//                    @Override
//                    public void onPositiveClick(View view, String selectedText) {
//                        tv_apartment.setText(selectedText);
//                    }
//                    @Override
//                    public void onNegativeClick(View view) {
//                    }
//                });
                break;
            case R.id.edit_info_base_rl_height:// 身高
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), true, DataPreference.getInchCmList(), mContext.getString(R.string.complete_height), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_height.setText(selectedText);
                        tv_height2.setText(selectedText);
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_base_rl_income:// 收入
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataPreference.getIncomeMapValue(), mContext.getString(R.string.earn), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_income.setText(selectedText);
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_base_rl_marital_status:// 婚姻状况
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataPreference.getRelationshipMapValue(), mContext.getString(R.string.marraige), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_marital_status.setText(selectedText);
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_rl_education:// 学历
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataPreference.getEducationMapValue(), mContext.getString(R.string.edit_info_qualification), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_education.setText(selectedText);
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_rl_job:// 工作
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataPreference.getOccupationMapValue(), mContext.getString(R.string.work), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_job.setText(selectedText);
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_rl_birthday://生日
                DialogUtil.showDateSelectDialog(getSupportFragmentManager(), mContext.getString(R.string.edit_info_birthday), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), false, new ThreeWheelDialog.OnThreeWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText1, String selectedText2, String selectedText3) {
                        tv_birthday.setText(selectedText1 + "-" + selectedText2 + "-" + selectedText3);
                        mEditInfoPresenter.getYearsAndMonth(selectedText1, selectedText2);
                    }

                    @Override
                    public void onNegativeClick(View view) {

                    }
                });
                break;
            case R.id.edit_info_rl_weight:// 体重
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataPreference.getWeight(), mContext.getString(R.string.edit_info_weight), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), false, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_weight.setText(selectedText + "kg");
                    }

                    @Override
                    public void onNegativeClick(View view) {
                    }
                });
                break;
            case R.id.edit_info_rl_sign:// 星座
                DialogUtil.showSingleWheelDialog(getSupportFragmentManager(), false, DataDictManager.getSignList(), mContext.getString(R.string.complete_signs), mContext.getString(R.string.setting_true), mContext.getString(R.string.cancel), true, new OneWheelDialog.OnOneWheelDialogClickListener() {
                    @Override
                    public void onPositiveClick(View view, String selectedText) {
                        tv_sign.setText(selectedText);
                    }

                    @Override
                    public void onNegativeClick(View view) {

                    }
                });
                break;
            case R.id.edit_info_base_rl_age:
                AgeSelectDialog.show(getSupportFragmentManager(), "22", new AgeSelectDialog.OnAgeSelectListener() {
                    @Override
                    public void onSelected(String age) {
                        tv_age.setText(age + mContext.getString(R.string.edit_info_years_old));
                        tv_age2.setText(age + mContext.getString(R.string.edit_info_years_old));
                    }
                });
                break;
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override

    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public String getIntroducation() {
        return null;
    }

    @Override
    public void setUserAvator(String photoUrl, String status) {
        if (!TextUtils.isEmpty(status)) {
            if (status.equals("1")) {
                iv_avatar_status.setVisibility(View.GONE);
            } else {
                iv_avatar_status.setVisibility(View.VISIBLE);
            }
        }
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(photoUrl)
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_avatar).build());

    }


    @Override
    public void setIntroducation(String introducation) {

    }


    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public String stringNickName() {
        return tv_nickname.getText().toString();
    }

    @Override
    public String stringHeight() {
        return tv_height.getText().toString();
    }

    @Override
    public String stringIncome() {
        return tv_income.getText().toString();
    }

    @Override
    public String stringRelation() {
        return tv_marital_status.getText().toString();
    }

    @Override
    public String stringEducation() {
        return tv_education.getText().toString();
    }

    @Override
    public String stringJob() {
        return tv_job.getText().toString();
    }

    @Override
    public String stringBirthday() {
        return tv_birthday.getText().toString();
    }

    @Override
    public String stringWeight() {
        return tv_weight.getText().toString();
    }

    @Override
    public String stringApartment() {
        return tv_apartment.getText().toString();
    }

    @Override
    public String stringSign() {
        return tv_sign.getText().toString();
    }

    @Override
    public String stringAge() {
        return tv_age.getText().toString();
    }

    @Override
    public void isMale(boolean male) {
        if (male) {
            rl_bg.setBackgroundResource(R.drawable.icon_male_background);
            rl_toobar.setBackgroundResource(R.drawable.icon_male_background);
            rl_sex.setBackgroundResource(R.drawable.icon_male);
            tv_sex.setText(mContext.getString(R.string.register_male));
        } else {
            rl_sex.setBackgroundResource(R.drawable.icon_famle);
            tv_sex.setText(mContext.getString(R.string.register_female));
        }
    }

    @Override
    public void getNickNmae(String name) {
        tv_nickname.setText(name);
        et_nickname.setText(name);
    }

    @Override
    public void getUserId(String id) {
        tv_id.setText("ID:" + id);
    }

    @Override
    public void getAge(String age) {
        tv_age.setText(age + mContext.getString(R.string.edit_info_years_old));
        tv_age2.setText(age + mContext.getString(R.string.edit_info_years_old));
    }

    @Override
    public void getHeight(String height) {
        tv_height.setText(height + "cm");
        tv_height2.setText(height + "cm");

    }

    @Override
    public void getApartment(String apartment) {
        tv_apartment.setText(apartment);
    }

    @Override

    public void getEducation(String education) {
        tv_education.setText(education);
    }

    @Override
    public void getIncome(String income) {
        tv_income.setText(income);
    }

    @Override
    public void getMarriage(String marriage) {
        tv_marital_status.setText(marriage);
    }

    @Override
    public void getJob(String job) {
        tv_job.setText(job);
    }

    @Override
    public void getBirthday(String birthday) {
        tv_birthday.setText(birthday);
    }

    @Override
    public void getWeight(String weight) {
        tv_weight.setText(weight + "kg");
    }

    @Override
    public void getSign(String sign) {
        tv_sign.setText(sign);
    }

    @Override
    public void setAdapter(AlbumPhotoAdapter adapter) {
        recyclerView_album.setAdapter(adapter);
    }

    @Override
    public void setGoneImageView() {
        recyclerView_album.setVisibility(View.VISIBLE);
    }

    @Override
    public void setStatus(String stauts) {
        tv_status.setText(stauts);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //触摸屏幕，可以让软键盘消失
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (this.getCurrentFocus() != null) {
                if (this.getCurrentFocus().getWindowToken() != null) {
                    imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
        return super.onTouchEvent(event);
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }

    @Override
    public void onBackPressed() {
        DialogUtil.showDoubleBtnDialog(getSupportFragmentManager(), "修改资料", "确认修改？", getString(R.string.confirm), getString(R.string.cancel), false, new OnDoubleDialogClickListener() {
            @Override
            public void onPositiveClick(View view) {
                mEditInfoPresenter.upLoadMyInfo();
            }

            @Override
            public void onNegativeClick(View view) {
                finish();
            }
        });
    }

    @Subscribe
    public void onEvent(NettyMessage event) {
        msgHandler.sendEmptyMessage(1);
    }

    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    RingManage.getInstance().closeRing();
                    break;
            }
        }
    };

    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }
}
