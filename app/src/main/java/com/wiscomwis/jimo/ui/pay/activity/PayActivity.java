package com.wiscomwis.jimo.ui.pay.activity;

import android.content.Intent;
import android.os.Parcelable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseFragmentActivity;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.AlipayInfo;
import com.wiscomwis.jimo.data.model.WeChatInfo;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.event.SingleLoginFinishEvent;
import com.wiscomwis.jimo.parcelable.PayInfoParcelable;
import com.wiscomwis.jimo.ui.pay.alipay.AlipayHelper;
import com.wiscomwis.jimo.wxapi.WXPayEntryActivity;
import com.wiscomwis.library.net.NetUtil;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/8/30.
 */

public class PayActivity extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.pay_activity_rl_alipay)
    RelativeLayout rl_alipay;
    @BindView(R.id.pay_activity_rl_wx_pay)
    RelativeLayout rl_wxpay;
    @BindView(R.id.pay_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.pay_activity_tv_purchase_product)
    TextView tv_service;
    @BindView(R.id.pay_activity_tv_purchase_money)
    TextView tv_money;
    private String payId="";
    private PayInfoParcelable payInfoParcelable;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_pay;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        payInfoParcelable= (PayInfoParcelable) parcelable;
        payId=payInfoParcelable.serviceId;
        tv_money.setText(payInfoParcelable.price);
        if(payInfoParcelable.type==2){
            tv_service.setText(getString(R.string.buy)+payInfoParcelable.serviceName+"VIP");
        }else if(payInfoParcelable.type==1){//钻石
            tv_service.setText(getString(R.string.buy)+payInfoParcelable.serviceName+getString(R.string.dialog_unit_ask_gift));
        }else if(payInfoParcelable.type==3){//钥匙
            tv_service.setText(getString(R.string.buy)+payInfoParcelable.serviceName+getString(R.string.key));
        }
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
    }

    @Override
    protected void setListeners() {
        rl_alipay.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_wxpay.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pay_activity_rl_alipay:
                ApiManager.alipayOrderInfo(UserPreference.getId(), payId, new IGetDataListener<AlipayInfo>() {
                    @Override
                    public void onResult(AlipayInfo baseModel, boolean isEmpty) {
                        if(baseModel!=null){
//                                EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX);//沙盒测试 上线时关闭
                            AlipayHelper.getInstance().callAlipayApi(mContext,baseModel.getAliPayOrder(),payInfoParcelable.serviceName,payInfoParcelable.price,payInfoParcelable.type);
                        }
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                    }
                });
                break;
            case R.id.pay_activity_rl_wx_pay:
                ApiManager.wxOrderInfo(UserPreference.getId(), payId, new
                        IGetDataListener<WeChatInfo>() {
                    @Override
                    public void onResult(WeChatInfo weChatInfo, boolean isEmpty) {
                        if (weChatInfo!=null) {
                            Intent intent=new Intent(PayActivity.this, WXPayEntryActivity.class);
                            intent.putExtra("type",payInfoParcelable.type);
                            intent.putExtra("serviceName",payInfoParcelable.serviceName);
                            intent.putExtra("price",payInfoParcelable.price);
                            intent.putExtra("orderInf",weChatInfo.getWeixinPayOrder());
                            PayActivity.this.startActivity(intent);
                        }
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {

                    }});
                break;
            case R.id.pay_activity_rl_back:
                finish();
                break;
        }
    }
    @Subscribe
    public void onEvent(SingleLoginFinishEvent event){
        finish();//单点登录销毁的activity
    }
}
