package com.wiscomwis.jimo.ui.personalcenter.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.preference.DataPreference;
import com.wiscomwis.jimo.ui.personalcenter.contract.AuthenticationContract;

import java.io.File;

/**
 * Created by Administrator on 2017/6/16.
 */

public class AuthenticationPresenter implements AuthenticationContract.IPresenter{
    private AuthenticationContract.IView mAuthenticationIview;
    private Context mContext;
    private File videoFile;
    public AuthenticationPresenter(AuthenticationContract.IView mAuthenticationIview) {
        this.mAuthenticationIview = mAuthenticationIview;
        this.mContext=mAuthenticationIview.obtainContext();
    }

    @Override
    public void start() {

    }


    @Override
    public void goVideoRecordPage() {

    }

    @Override
    public void getUserInfo() {
    }


    @Override
    public void startRecord() {


    }

    @Override
    public void startPlay() {

    }

    @Override
    public void reRecord() {

    }

    @Override
    public void getFaceBookAccount() {
//        String middleImage = UserPreference.getMiddleImage();
//        if(!TextUtils.isEmpty(middleImage)){
//            mAuthenticationIview.getAvatarUrl(middleImage);
//        }
        mAuthenticationIview.setFaceBookAccount(DataPreference.getfacebookaccount());
    }

    @Override
    public void submitCheck(String videoNumber, File videoFile,File avatarFile) {
        if(!TextUtils.isEmpty(videoNumber)&&videoFile!=null&&avatarFile!=null){
            mAuthenticationIview.showLoading();
            ApiManager.submitCheck(videoNumber,videoFile,avatarFile, new IGetDataListener<String>() {
                @Override
                public void onResult(String s, boolean isEmpty) {
//                    Toast.makeText(mContext, mContext.getString(R.string.authentication_wait_submit), Toast.LENGTH_SHORT).show();
                    mAuthenticationIview.dismissLoading();
                    mAuthenticationIview.finishActivity();
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }
            });
        } else{
            mAuthenticationIview.showTip(mContext.getString(R.string.upload_video_first));
        }
    }


}
