package com.wiscomwis.jimo.ui.detail.presenter;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.ParamsUtils;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.MyInfo;
import com.wiscomwis.jimo.data.model.UpLoadMyInfo;
import com.wiscomwis.jimo.data.model.UpLoadMyPhoto;
import com.wiscomwis.jimo.data.model.UploadInfoParams;
import com.wiscomwis.jimo.data.model.UserBase;
import com.wiscomwis.jimo.data.model.UserBean;
import com.wiscomwis.jimo.data.model.UserDetail;
import com.wiscomwis.jimo.data.model.UserMend;
import com.wiscomwis.jimo.data.model.UserPhoto;
import com.wiscomwis.jimo.data.preference.DataPreference;
import com.wiscomwis.jimo.data.preference.PayPreference;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.event.UserInfoChangedEvent;
import com.wiscomwis.jimo.parcelable.BigPhotoParcelable;
import com.wiscomwis.jimo.ui.detail.adapter.AlbumPhotoAdapter;
import com.wiscomwis.jimo.ui.detail.contract.EditInfoContract;
import com.wiscomwis.jimo.ui.photo.BigPhotoActivity;
import com.wiscomwis.library.adapter.MultiTypeRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.util.LaunchHelper;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

/**
 * Created by Administrator on 2017/6/9.
 */
public class EditInfoPresenter implements EditInfoContract.IPresenter {
    private EditInfoContract.IView mEditInfoView;
    private Context mContext;
    private String languageString = null;
    private AlbumPhotoAdapter mAlbumPhotoAdapter;

    public EditInfoPresenter(EditInfoContract.IView mEditInfoView) {
        this.mEditInfoView = mEditInfoView;
        mContext = mEditInfoView.obtainContext();
    }

    @Override
    public void start() {
        mEditInfoView.isMale(UserPreference.isMale());
        mAlbumPhotoAdapter = new AlbumPhotoAdapter(mContext, R.layout.album_recyclerview_item, 0);
        mEditInfoView.setAdapter(mAlbumPhotoAdapter);
        mAlbumPhotoAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                final List<UserPhoto> adapterDataList = mAlbumPhotoAdapter.getAdapterDataList();
                if(adapterDataList!=null&&adapterDataList.size()>0){
                    // 点击图片查看大图
                    LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(position,
                            Util.convertPhotoUrl(adapterDataList)));
                }
            }
        });
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        List<UserPhoto> userPhotos = userDetail.getUserPhotos();
                        if (userPhotos != null && userPhotos.size() > 0) {
                            mEditInfoView.setGoneImageView();
                            mAlbumPhotoAdapter.bind(userPhotos);
                        }
                        UserBase userBase = userDetail.getUserBase();
                        UserBean userBean = userDetail.getUserBean();
                        if(userBean!=null){
                            PayPreference.saveDionmadsNum(userBean.getCounts());
                        }
                        if (userBase != null) {
                            mEditInfoView.setUserAvator(userBase.getIconUrlMiddle(),userBase.getIconStatus());
                            mEditInfoView.getUserId(String.valueOf(userBase.getAccount()));
                            mEditInfoView.getAge(String.valueOf(userBase.getAge()));
                            mEditInfoView.getApartment(userBase.getState());
                            mEditInfoView.getNickNmae(userBase.getNickName());
                            mEditInfoView.getSign(ParamsUtils.getSignValue(String.valueOf(userBase.getSign())));
                            mEditInfoView.getBirthday(userBase.getBirthday());
                            String state = userBase.getStatus();
                            if (!TextUtils.isEmpty(state)) {
                                if (state.equals("3")) {
                                    mEditInfoView.setStatus("勿扰");
                                } else if (state.equals("1")) {
                                    mEditInfoView.setStatus("在线");
                                } else {
                                    mEditInfoView.setStatus("在线");
                                }
                            }

                        }
                        UserMend userMend = userDetail.getUserMend();
                        if (userMend != null) {
                            if (!TextUtils.isEmpty(userMend.getHeight())) {
                                mEditInfoView.getHeight(userMend.getHeight());
                            } else {
                                mEditInfoView.getHeight("173");
                            }
                            mEditInfoView.getEducation(userMend.getEducationId());
                            if (!TextUtils.isEmpty(userMend.getWeight())) {
                                mEditInfoView.getWeight(userMend.getWeight());
                            }
                            if (!TextUtils.isEmpty(userMend.getEducationId())) {
                                mEditInfoView.getEducation(DataPreference.getValueByKey(userMend.getEducationId(), 0));
                            } else {
                                mEditInfoView.getEducation("未填写");
                            }
                            if (!TextUtils.isEmpty(userMend.getRelationshipId())) {
                                mEditInfoView.getMarriage(DataPreference.getValueByKey(userMend.getRelationshipId(), 1));
                            } else {
                                mEditInfoView.getMarriage("未填写");
                            }
                            if (!TextUtils.isEmpty(userMend.getIncomeId())) {
                                mEditInfoView.getIncome(DataPreference.getValueByKey(userMend.getIncomeId(), 2));
                            } else {
                                mEditInfoView.getIncome("未填写");
                            }
                            if (!TextUtils.isEmpty(userMend.getOccupationId())) {
                                mEditInfoView.getJob(DataPreference.getValueByKey(userMend.getOccupationId(), 3));
                            } else {
                                mEditInfoView.getJob("未填写");
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }


    @Override
    public void setLocalInfo() {
//        mEditInfoView.setUserAvator(UserPreference.getMiddleImage());
        mEditInfoView.setIntroducation(UserPreference.getIntroducation());
    }

    @Override
    public void upLoadMyInfo() {
        mEditInfoView.showLoading();
        UploadInfoParams uploadInfoParams = new UploadInfoParams();
        if (!TextUtils.isEmpty(mEditInfoView.stringNickName())) {
            uploadInfoParams.setNickName(mEditInfoView.stringNickName());
        }
        if (!TextUtils.isEmpty(mEditInfoView.stringHeight())) {
            String height = mEditInfoView.stringHeight().replace("cm", "");
            uploadInfoParams.setHeight(height);
        }
        if (!TextUtils.isEmpty(mEditInfoView.stringIncome())) {
            uploadInfoParams.setIncomeId(DataPreference.getPersonMapKey(mEditInfoView.stringIncome(), 2));
        }
        if (!TextUtils.isEmpty(mEditInfoView.stringRelation())) {
            uploadInfoParams.setRelationshipId(DataPreference.getPersonMapKey(mEditInfoView.stringRelation(), 1));
        }
        if (!TextUtils.isEmpty(mEditInfoView.stringEducation())) {
            uploadInfoParams.setEducationId(DataPreference.getPersonMapKey(mEditInfoView.stringEducation(), 0));
        }
        if (!TextUtils.isEmpty(mEditInfoView.stringJob())) {
            uploadInfoParams.setOccupationId(DataPreference.getPersonMapKey(mEditInfoView.stringJob(), 3));
        }
        if (!TextUtils.isEmpty(mEditInfoView.stringBirthday())) {
            //    uploadInfoParams.setBirthday(mEditInfoView.stringBirthday());
        }
        if (!TextUtils.isEmpty(mEditInfoView.stringWeight())) {
            String kg = mEditInfoView.stringWeight().replace("kg", "");
            uploadInfoParams.setWeight(kg);
        }

        if (!TextUtils.isEmpty(mEditInfoView.stringSign())) {
            uploadInfoParams.setSign(ParamsUtils.getSignkey(mEditInfoView.stringSign()));
        }
        if(!TextUtils.isEmpty(mEditInfoView.stringAge())){
            String substring = mEditInfoView.stringAge().substring(0,2);
            uploadInfoParams.setAge(substring);
        }
        ApiManager.upLoadMyInfo(uploadInfoParams, new IGetDataListener<UpLoadMyInfo>() {
            @Override
            public void onResult(UpLoadMyInfo upLoadMyInfo, boolean isEmpty) {
                // mEditInfoView.dismissLoading();
                // 发送用户资料改变事件
                EventBus.getDefault().post(new UserInfoChangedEvent());
                if (upLoadMyInfo != null) {
                    UserBase userBase = upLoadMyInfo.getUserBase();
                    if (userBase != null) {
                        // 更新本地用户信息
                        UserPreference.saveUserInfo(userBase);
                    }
                }
                // finish
                mEditInfoView.finishActivity();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mEditInfoView.dismissLoading();
                if (isNetworkError) {
                    mEditInfoView.showNetworkError();
                } else {
                    mEditInfoView.showTip(msg);
                }
            }
        });
    }

    @Override
    public void upLoadAvator(File file, boolean photoType) {
        mEditInfoView.showLoading();
        ApiManager.upLoadMyPhotoOrAvator(file, photoType, new IGetDataListener<UpLoadMyPhoto>() {
            @Override
            public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                mEditInfoView.dismissLoading();
                if (upLoadMyPhoto != null) {
                    final UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                    if (userPhoto != null) {
                        final String stringFile = userPhoto.getFileUrlMiddle();
                        String fileUrl = userPhoto.getFileUrl();
                        String fileUrlMinimum = userPhoto.getFileUrlMinimum();
                        if (!TextUtils.isEmpty(stringFile)) {
                            UserPreference.setOriginalImage(fileUrl);
                            UserPreference.setMiddleImage(stringFile);
                            UserPreference.setSmallImage(fileUrlMinimum);
                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    mEditInfoView.setUserAvator(stringFile,userPhoto.getStatus());
                                }
                            }, 3000);
                        }
                    }
                }
                mEditInfoView.showTip(mContext.getString(R.string.person_picture_check));
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mEditInfoView.dismissLoading();
                if (isNetworkError) {
                    mEditInfoView.showNetworkError();
                } else {
                    mEditInfoView.showTip(msg);
                }
            }
        });
    }

    @Override
    public void getUserInfo() {

    }

    @Override
    public void getYearsAndMonth(String year, String month) {
        if(!TextUtils.isEmpty(year)&&year.length()>0){
            int i = Integer.parseInt(year);
            int years=2017- i;
            mEditInfoView.getAge(String.valueOf(years));
        }
        if (!TextUtils.isEmpty(month)&&month.length()>0) {
            int i = Integer.parseInt(month);
            mEditInfoView.getSign(ParamsUtils.getSignValue(String.valueOf(i-1)));
        }
    }

}
