package com.wiscomwis.jimo.base;

import android.app.Application;
import android.content.Context;

import com.wiscomwis.jimo.common.HyphenateHelper;
import com.wiscomwis.jimo.db.DbModle;
import com.wiscomwis.jimo.service.InitializeService;

/**
 * Created by zhangdroid on 2017/5/11.
 */
public class BaseApplication extends Application {
    private static Context sApplicationContext;

    public static Context getGlobalContext() {
        return sApplicationContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplicationContext = getApplicationContext();
        // 初始化环信
        HyphenateHelper.getInstance().init(this);
        InitializeService.start(this);
        DbModle.getInstance().init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

}
