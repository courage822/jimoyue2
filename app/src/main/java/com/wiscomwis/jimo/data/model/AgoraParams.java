package com.wiscomwis.jimo.data.model;

/**
 * 声网信令系统呼叫时需要传递的参数对象
 * Created by zhangdroid on 2017/6/15.
 */
public class AgoraParams {
    /**
     * 被邀请人id
     */
    private long uId;
    /**
     * 邀请人昵称
     */
    private String nickname;
    /**
     * 被邀请人头像
     */
    private String imageUrl;
    /**
     * 消息类型，0为视频，1为语音
     */
    private int type;
    /**
     * 0是呼叫，1为直接接通
     */
    private String start_time;
    private int inviteType;
    public AgoraParams(long uId, String nickname, String imgageUrl,int type,int inviteType) {
        this.uId = uId;
        this.nickname = nickname;
        this.imageUrl = imgageUrl;
        this.type=type;
        this.inviteType=inviteType;
    }

    public int getInviteType() {
        return inviteType;
    }

    public void setInviteType(int inviteType) {
        this.inviteType = inviteType;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getuId() {
        return uId;
    }

    public void setuId(long uId) {
        this.uId = uId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getImgageUrl() {
        return imageUrl;
    }

    public void setImgageUrl(String imgageUrl) {
        this.imageUrl = imgageUrl;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    @Override
    public String toString() {
        return "AgoraParams{" +
                "uId=" + uId +
                ", nickname='" + nickname + '\'' +
                ", imgageUrl='" + imageUrl + '\'' +
                '}';
    }
}
