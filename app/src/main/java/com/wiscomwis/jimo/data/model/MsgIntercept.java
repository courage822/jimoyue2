package com.wiscomwis.jimo.data.model;

/**
 * Created by WangYong on 2018/2/28.
 */

public class MsgIntercept extends BaseModel{
    private int payIntercept;

    public int getPayIntercept() {
        return payIntercept;
    }

    public void setPayIntercept(int payIntercept) {
        this.payIntercept = payIntercept;
    }
}
