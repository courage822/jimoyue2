package com.wiscomwis.jimo.data.model;

/**
 * Created by WangYong on 2018/2/27.
 */

public class CheckStatus extends BaseModel{
    private int auditStatus;//0、审核中 ,1、审核通过 ,-1 审核失败 ,-2未提交审核

    public CheckStatus(int auditStatus) {
        this.auditStatus = auditStatus;
    }

    public int getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(int auditStatus) {
        this.auditStatus = auditStatus;
    }

    @Override
    public String toString() {
        return "CheckStatus{" +
                "auditStatus=" + auditStatus +
                '}';
    }
}
