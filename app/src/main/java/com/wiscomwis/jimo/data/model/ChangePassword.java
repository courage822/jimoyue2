package com.wiscomwis.jimo.data.model;

/**
 * Created by WangYong on 2018/2/27.
 */

public class ChangePassword extends BaseModel{
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
