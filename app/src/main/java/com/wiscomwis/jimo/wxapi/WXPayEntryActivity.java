package com.wiscomwis.jimo.wxapi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseTopBarActivity;
import com.wiscomwis.jimo.common.CustomDialogAboutPay;
import com.wiscomwis.jimo.data.model.WeChat;
import com.wiscomwis.jimo.event.FinishWxActivity;
import com.wiscomwis.library.net.NetUtil;

import org.greenrobot.eventbus.Subscribe;

public class WXPayEntryActivity extends BaseTopBarActivity implements IWXAPIEventHandler {
    // IWXAPI 是第三方app和微信通信的openapi接口
    private IWXAPI api;
    private String payInfo = "";
    private int type = 1;
    private String serviceName = "";
    private String price = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_wxpay_entry;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return null;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        Intent intent = getIntent();
        if (intent != null) {
            payInfo = intent.getStringExtra("orderInf");
            serviceName = intent.getStringExtra("serviceName");
            price = intent.getStringExtra("price");
            type = intent.getIntExtra("type", 1);
        }
        // 通过WXAPIFactory工厂，获取IWXAPI的实例
        api = WXAPIFactory.createWXAPI(this, "wx4d2195f94f906c09", false);
        // 将该app注册到微信
        api.registerApp("wx4d2195f94f906c09");
        api.handleIntent(getIntent(), this);
        if (!TextUtils.isEmpty(payInfo)) {
            WeChat weChat = new Gson().fromJson(payInfo, WeChat.class);
            if (weChat != null) {
                PayReq req = new PayReq();
                req.appId = "wx4d2195f94f906c09";
                req.partnerId = weChat.getPartnerid();
                req.prepayId = weChat.getPrepayid();
                req.nonceStr = weChat.getNoncestr();
                req.timeStamp = weChat.getTimestamp();
                req.packageValue = weChat.getPackageX();
                req.sign = weChat.getSign();
                req.extData = "app data"; // optional
                // 在支付之前，如果应用没有注册到微信，应该先调用IWXMsg.registerApp将应用注册到微信
                api.sendReq(req);
            }
        }
    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    // 微信发送请求到第三方应用时，会回调到该方法
    @Override
    public void onReq(BaseReq baseReq) {


    }


    // 第三方应用发送到微信的请求处理后的响应结果，会回调到该方法
    @Override
    public void onResp(BaseResp resp) {
        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            if (resp.errCode == 0) {
                CustomDialogAboutPay.paySucceedShow(WXPayEntryActivity.this, serviceName, price, type);
            } else {
                CustomDialogAboutPay.payFaildShow(WXPayEntryActivity.this);
            }
        }

    }

    @Subscribe
    public void onEvent(FinishWxActivity finish) {
        finish();
    }

}