package com.wiscomwis.library.adapter.base.entity;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public interface MultiItemEntity {
    int getItemType();
}
